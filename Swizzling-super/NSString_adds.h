//
//  NSString_adds.h
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (adds)
- (NSRange)range;
- (NSArray *)match:(NSString *)pattern;
- (NSArray *)match:(NSString *)pattern options:(NSRegularExpressionOptions)opt;

@end
