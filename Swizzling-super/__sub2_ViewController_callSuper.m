//
//  __sub2_ViewController.m
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import "__sub2_ViewController_callSuper.h"

@interface __sub2_ViewController_callSuper ()

@end

@implementation __sub2_ViewController_callSuper

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%@==BEFORE %s",(^{
        
        NSMutableString *s = [NSMutableString new];
        Class _cl = self.class;
        while ( _cl != [UIViewController class] ) {
            [s appendString:@"="];
            _cl = [_cl superclass];
        }
        return s;
    })(), __PRETTY_FUNCTION__);    [super viewWillAppear:animated];
    NSLog(@"%@==AFTER %s",(^{
        
        NSMutableString *s = [NSMutableString new];
        Class _cl = self.class;
        while ( _cl != [UIViewController class] ) {
            [s appendString:@"="];
            _cl = [_cl superclass];
        }
        return s;
    })(), __PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
