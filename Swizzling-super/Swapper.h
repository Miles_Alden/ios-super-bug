//
//  Swapper.h
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Swapper : NSObject
+ (void)swap;

@end
