//
//  UIViewController_adds.m
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import "UIViewController_adds.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation UIViewController (adds)
@dynamic nstn_superTracker;

static void *nstn_superTrackerKey;


// Nib Key for runtime values from nibs
- (void)setNstn_superTracker:(NSString *)superTracker {
    objc_setAssociatedObject(self, &nstn_superTrackerKey, superTracker, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSString *)nstn_superTracker {
    return objc_getAssociatedObject(self, &nstn_superTrackerKey);
}

@end
