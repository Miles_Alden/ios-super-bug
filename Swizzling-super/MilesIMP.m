//
//  MilesIMP.m
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import "MilesIMP.h"
#import <objc/runtime.h>
#import <objc/message.h>
#import <UIKit/UIKit.h>
#import "NSString_adds.h"
#import "UIViewController_adds.h"


@implementation MilesIMP
NSMutableArray *getSuperChain (id obj, Class baseOrNil) {
    
    NSMutableArray *chain = [NSMutableArray new];
    Class _super = object_getClass(obj);
    
    while ( _super != baseOrNil ) {
        // Prepend each item
        [chain insertObject:NSStringFromClass(_super) atIndex:0];
        _super = class_getSuperclass(_super);
    }
    
    if (baseOrNil)
        [chain insertObject:NSStringFromClass(baseOrNil) atIndex:0];
    
    return chain;
}
struct objc_super getSuper(id obj, Class lastClass) {
    
    Class class;
    if ( !lastClass )
        class = class_getSuperclass(object_getClass(obj));
    else
        class = class_getSuperclass(lastClass);
    
    
    struct objc_super mySuper = {
        .receiver = obj,
        .super_class = class_isMetaClass(object_getClass(obj)) //check if we are an instance or Class
        ? object_getClass(class)                //if we are a Class, we need to send our metaclass (our Class's Class)
        : class                                 //if we are an instance, we need to send our Class (which we already have)
    };
    
    return mySuper;
    
}

void invokeSuper(struct objc_super mySuper, SEL selector, BOOL animated) {
    
    void (*objc_superAllocTyped)(struct objc_super *, SEL, BOOL) = (void *)&objc_msgSendSuper; //cast our pointer so the compiler can sort out the ABI
    (*objc_superAllocTyped)(&mySuper, selector, animated);
}
- (void)viewWillOrDidSomething:(BOOL)animated {
    
    NSLog(@"\n\n\t==Intercepted -[%@ %@]==\n\n", self.class, NSStringFromSelector(_cmd));
    
    NSString* swizzledSEL = [@"original_" stringByAppendingString:NSStringFromSelector(_cmd)];
    SEL newSEL = NSSelectorFromString(swizzledSEL);

    Class forwardingClass;
    BOOL isSuperCall = NO, isNextCallImp = NO;
    struct objc_super _super;
    
    NSArray *superChain = getSuperChain (self, [UIViewController class]);
    
    IMP _thisIMP = class_getMethodImplementation( NSClassFromString(@"MilesIMP"), @selector(viewWillOrDidSomething:) );

    
    
    NSArray *addresses = [NSThread callStackReturnAddresses];
    NSArray *syms = [NSThread callStackSymbols];
    for ( int i = 0; i < addresses.count; i++) {
        
        NSArray *matches = [syms[i] match:@" \\-\\[[^ ]* (viewWill|viewDid)Appear:\\]"];
        if ( matches.count > 0 ) {

            // Regex extract class
            NSString *_clStr = [[matches[0] substringFromIndex:3] componentsSeparatedByString:@" "][0];
            
            // Check if we have already set a superclass and use it
            if ( [(id)self nstn_superTracker] ) {
                if ( [_clStr isEqualToString:[(id)self nstn_superTracker]] ) {
                    forwardingClass = [NSClassFromString( [(id)self nstn_superTracker] ) superclass];
                }
            }
            
            // If none existed, assume this is our super
            // starting point. Grab its superclass and work from there.
            if ( !forwardingClass )
                forwardingClass = [NSClassFromString(_clStr) superclass];
            
            
            // Prevents calling "self" when something is tagged
            // later on the chain as well as this instance
            IMP _nextIMP = class_getMethodImplementation( self.class, _cmd );
            isNextCallImp = _thisIMP == _nextIMP;
            NSLog(@"Next call is this imp : %d", isNextCallImp );
            if ( isNextCallImp ) break;
            
            // Base class for any ViewController is UIViewController
            // ** unless someone is a complete ass **
            if ( forwardingClass != [UIViewController class] ) {
                
                NSLog(@"Forwarding class is %@", forwardingClass );

                // Set our next superclass for next reference
                [(id)self setNstn_superTracker:NSStringFromClass(forwardingClass)];

                // Flag that we need to call super instead
                isSuperCall = YES;
                
                // Generate super object
                struct objc_super mySuper = {
                    .receiver = self,
                    .super_class = forwardingClass                                //if we are an instance, we need to send our Class (which we already have)
                };
                
                // Set to larger scope
                _super = mySuper;
                
                // Found it. Bail on search
                break;
            }
        }
    }
    

    
    /*
    // Find spot in chain
    NSMutableString *tabs = [NSMutableString new];
    for ( int i = (int)superChain.count-1; i > -1; i-- ) {
        Class _sClass = NSClassFromString(superChain[i]);
        imps[i] = class_getMethodImplementation( _sClass, _cmd );
        NSLog(@"%@ IMP for class %@ is <%p>", tabs, _sClass, imps[i]);
        //        NSLog(@"%@ IMP <%p> %@ <%p>", tabs, _thisIMP, ( _thisIMP == imps[i] ) ? @"==" : @"!=", imps[i] );
        if ( _thisIMP == imps[i] && _sClass != [UIViewController class] ) {
            // Found this method somewhere down the line
            // Will cause an eventual recursion...bad.
            // Shitty way to break out of this...repurcussions?
            
            NSLog(@"%@ IMP for class %@ is <%p> will be recursed upon...", tabs, _sClass, imps[i]);
            if (!forwardingClass)
                forwardingClass = NSClassFromString(superChain[i-1]);
            
            isNextCallImp_orIsSuperCall = YES;
            struct objc_super mySuper = {
                .receiver = self,
                .super_class = forwardingClass                                //if we are an instance, we need to send our Class (which we already have)
            };
            _super = mySuper;
            break;
        }
        [tabs appendString:@"  "];
        
    }
     */
    
    
    /*
     // Next attempt
     if ( [superChain indexOfObject:NSStringFromClass(self.class)] )
     _cl = NSClassFromString(superChain[1]);
     else
     _cl = NSClassFromString(superChain[0]);
     
     _super = getSuper(self, _cl );
     
     IMP _origIMP = class_getMethodImplementation( _cl, _cmd );
     
     isNextCallImp_orIsSuperCall = ( _origIMP && _origIMP == _thisIMP );
     if ( isNextCallImp_orIsSuperCall ) {
     NSLog(@"Call to imp from %@. Rerouting to %@", [self class], _super.super_class);
     }
     */
    
    /*
     // Original attempt
     NSArray *syms = [NSThread callStackSymbols];
     NSString *sym = syms[1];
     NSArray *matches = [sym match:@" \\-\\[[^ ]* (viewWill|viewDid)Appear:\\]"];
     if ( matches.count > 0 ) {
     NSLog(@"Matches found : %@", matches);
     NSString *_clStr = [[matches[0] substringFromIndex:3] componentsSeparatedByString:@" "][0];
     Class _cl = NSClassFromString(_clStr);
     if ( _cl && class_getSuperclass(_cl) != [UIViewController class] ) {
     
     if ( _cl != [self class] )
     _super = getSuper(self, _cl);
     else
     _super = getSuper(self, NULL);
     
     NSLog(@"Assumed starting class %@ : self.class %@. Assumed super call.", _cl, [self class]);
     isSuperCall = YES;
     }
     }
     */
    
    
    
    if ( YES == [self respondsToSelector:newSEL] ) {
        
        if ( !isSuperCall && !isNextCallImp ) {
            
            NSLog(@"TAG : Tags go here for specific classes -[%@ %@%@]", self.class, NSStringFromSelector(_cmd), (animated) ? @"YES" : @"NO");
            
            void (*runAction)(id, SEL, BOOL) = (void (*)(id, SEL, BOOL)) objc_msgSend;
            runAction(self, NSSelectorFromString(swizzledSEL), animated);
            
            
        } else if ( isNextCallImp ) {
            
            struct objc_super mySuper = {
                .receiver = self,
                .super_class = forwardingClass                                //if we are an instance, we need to send our Class (which we already have)
            };
            _super = mySuper;
            
            if ( class_respondsToSelector(_super.super_class, newSEL) ) {
                void (*super_viewWillAppear)(struct objc_super *, SEL, BOOL) = (void *)&objc_msgSendSuper; //cast our pointer so the compiler can sort out the ABI
                super_viewWillAppear(&_super, newSEL, animated);
            }
        
        } else {
            
            NSLog(@"Attempting to invoke super : { receiver : %@, super-class : %@ } invoking %@", [_super.receiver class], _super.super_class, swizzledSEL );
            
            if ( [_super.super_class respondsToSelector:newSEL] ) {
                void (*super_viewWillAppear)(struct objc_super *, SEL, BOOL) = (void *)&objc_msgSendSuper; //cast our pointer so the compiler can sort out the ABI
                super_viewWillAppear(&_super, newSEL, animated);
            }

        }
        
    }
    
    NSLog(@"Exiting [%@ %@]", self.class, NSStringFromSelector(_cmd));
}

- (void)placeholderIMP {
    // do nothing
}

@end
