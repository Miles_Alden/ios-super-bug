//
//  NSString_adds.m
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import "NSString_adds.h"

@implementation NSString (adds)
- (NSRange)range {
    return NSMakeRange(0, self.length);
}
- (NSArray *)match:(NSString *)pattern {
    return [self match:pattern options:0];
}
- (NSArray *)match:(NSString *)pattern options:(NSRegularExpressionOptions)opt {
    
    if (opt < 1) opt = NSRegularExpressionAnchorsMatchLines;
    NSError *err;
    NSRegularExpression *r = [NSRegularExpression regularExpressionWithPattern:pattern options:opt error:&err];
    NSMutableArray *a = [NSMutableArray new];
    for ( NSTextCheckingResult *match in [r matchesInString:self options:NSMatchingReportCompletion range:[self range]] ) {
        [a addObject:[self substringWithRange:match.range]];
    }
    return (a.count < 1) ? nil : a;
}

@end
