//
//  main.m
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Swapper.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        [Swapper swap];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
