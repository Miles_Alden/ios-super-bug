//
//  Swapper.m
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import "Swapper.h"
#import "MilesIMP.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import <objc/message.h>

@implementation Swapper

+ (void)swap {

    [Swapper swapOne:@"UIViewController" sel:@"viewWillAppear:"];
    [Swapper swapOne:@"ViewController_noSuper" sel:@"viewWillAppear:"];
    [Swapper swapOne:@"ViewController_callSuper" sel:@"viewWillAppear:"];
    [Swapper swapOne:@"__sub2_ViewController_callSuper" sel:@"viewWillAppear:"];
    [Swapper swapOne:@"___sub3_ViewController_callSuper" sel:@"viewWillAppear:"];
    [Swapper swapOne:@"____sub4_ViewController_noIMP" sel:@"viewWillAppear:"];



}

+ (void)swapOne:(NSString *)sourceClassStr sel: (NSString *)sel {
    
    Class destClass = [MilesIMP class];
    Class sourceClass = NSClassFromString(sourceClassStr);
    
    NSLog(@"Swizzling [%@ %@] -> [%@ %@]", sourceClassStr, sel, destClass, @"viewWillOrDidSomething:");
    
    SEL selector = NSSelectorFromString(sel);
    
    // Replace default method IMP in UIViewController
    SEL placeHolderSel = NSSelectorFromString([@"original_" stringByAppendingString:sel]);
    
    // Get method and impl for existing methods
    Method theOriginalMethod = class_getInstanceMethod(sourceClass, selector);
    IMP theOriginalImplementation = method_getImplementation(theOriginalMethod);
    
    if ( !theOriginalMethod || !theOriginalImplementation || ![sourceClass respondsToSelector:selector] ) {
        
        // Use empty placeholder imp
        Method newMethod = class_getInstanceMethod(destClass, NSSelectorFromString(@"placeholderIMP"));
        IMP newIMP = method_getImplementation(newMethod);
        
        // Add method pointing to empty imp
        class_addMethod(sourceClass, selector, newIMP, method_getTypeEncoding(newMethod));
        
        // Reset method and imp vars
        theOriginalMethod = class_getInstanceMethod(sourceClass, selector);
        theOriginalImplementation = method_getImplementation(theOriginalMethod);
    }
    
    if ( [Swapper hasSwizzledToEnsightenIMP:NSStringFromClass(destClass) destSelector:sel] ) {
        NSLog(@"Swizzle already occurred");
        return;
    }
    // Add the "new" method that will just point to the original method/impl
    // NOTE: sel_registerName isn't explicitly called as it is part of NSSelectorFromString
    class_addMethod(sourceClass, placeHolderSel, theOriginalImplementation, method_getTypeEncoding(theOriginalMethod));
    
    Method milesMethod = class_getInstanceMethod(destClass, @selector(viewWillOrDidSomething:));
    IMP milesIMP = method_getImplementation(milesMethod);
    
    method_setImplementation(theOriginalMethod, milesIMP);
    
}

+ (BOOL)hasSwizzleOccurred:(NSString*)sourceSelectorStr destSelector:(NSString*)destSelectorStr sourceClass:(NSString*)sourceClassStr destClass:(NSString*)destClassStr {
    
    IMP targetIMP = class_getMethodImplementation(objc_getClass([sourceClassStr UTF8String]), NSSelectorFromString(sourceSelectorStr));
    IMP destIMP = class_getMethodImplementation(objc_getClass([destClassStr UTF8String]), NSSelectorFromString(destSelectorStr));
    
    return ( targetIMP == destIMP );
    
}

+ (BOOL)hasSwizzledToEnsightenIMP:(NSString*)_class destSelector:(NSString*)_sel {
    
    return [Swapper hasSwizzleOccurred:_sel destSelector:@"viewWillOrDidSomething:" sourceClass:_class destClass:@"MilesIMP"];
    
}

@end
