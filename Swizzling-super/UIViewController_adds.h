//
//  UIViewController_adds.h
//  Swizzling-super
//
//  Created by Miles Alden on 8/18/14.
//  Copyright (c) 2014 ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (adds)

@property (strong, nonatomic) NSString *nstn_superTracker;

@end
